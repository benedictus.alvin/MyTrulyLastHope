from django.shortcuts import render
import time
# Enter your name here
mhs_name = 'Benedictus Alvin' # TODO Implement this
mhs_birth = 1999

# Create your views here.
def index(request):
    response = {'name': mhs_name,'age' : calculate_age(mhs_birth)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    year_now = time.strftime("%Y")
    year_now2 = int(year_now)
    return year_now2 - birth_year

